const express   = require('express');
const app       = express();
const http      = require('http');
const socket = require("socket.io");


const  port = process.env.NODE_PORT || 3030;

const  httpServer = http.createServer(app);

httpServer.listen(port, function(){
    console.log('listening on *:' + port);
});

app.get('/', function (request, response) {
    response.sendFile(__dirname + '/public/index.html');
});

// Array with all connections
var connections = [];

var io = socket(httpServer, { /* ... */ });

io.on('connection', function(client){
    console.log("Client connected... ID: "+ client.id);

    // Add new connection to the array (at the end)
    connections.push(client);
    //console.log("Connection count: "+ connections.length);

    // If user was disconnected
    client.on('disconnect', function(data) {
        // Delete connection from the array
        connections.splice(connections.indexOf(client), 1);
        console.log("Client disconnected...");
        //console.log("Connection count: "+ connections.length);
    });

    // Send Message with parameters to the 'chat_id' or creates new 'chat_id'
    // Get Messages by 'chat_id' or without it & parameters
    client.on('sendMessage', function(data) {

        //console.log('sendMessage data: '+ JSON.stringify(data));

        let data_msg = {
            status:       200,

            result: {
                chat_id:    data.chat_id,
                message:    data.message,
                className:  data.className,
            },
        };

        io.emit('sendMessage', data_msg);
    });

});
